import java.util.Scanner;

public class Shop {
    public static void main(String[] arg){
        Scanner key = new Scanner(System.in);
        Apple[] crate = new Apple[4];

        for(int i = 0; i < crate.length; i++){
            int j = i + 1;

            crate[i] = new Apple();

            System.out.println("Enter the color of the apple number " + j);
            crate[i].color = key.next();
            System.out.println("Enter the size of the apple number " + j);
            crate[i].size = key.nextInt();
            System.out.println("Enter the how old in days is the apple number " + j);
            crate[i].old = key.nextInt();
            crate[i].fresh = true;
        }

        System.out.println(crate[3].color);
        System.out.println(crate[3].size);
        System.out.println(crate[3].old);
        System.out.println(crate[3].fresh);
        crate[3].isNotFresh();
    }
}
